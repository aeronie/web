This is an extremely sophisticated tool we use to build [our website](http://aeronie.fr/).

To build and test the site, you can:

* Go to the project directory
* Run `./make.py`
* Run `./test.sh` and visit `http://localhost:12345/`
* Upload the generated HTML, XML and JSON files to your FTP
