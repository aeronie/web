<?php

$a = array('fr' => 0, 'en' => 0);
$n = preg_match_all('!(\w+)[^,=]*(?:=\s*([\d\.]*))?!', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $matches);
for($i = 0; $i < $n; ++$i)
{
	$lang = $matches[1][$i];
	$q = $matches[2][$i];
	if(array_key_exists($lang, $a))
	{
		if($q)
		{
			if($a[$lang] < (float)$q)
			{
				$a[$lang] = (float)$q;
			}
		}
		else
		{
			$a[$lang] = 1;
		}
	}
}

$best_lang = 'en';
$best_q = 0;
foreach($a as $lang => $q)
{
	if($best_q < $q)
	{
		$best_lang = $lang;
		$best_q = $q;
	}
}

header('Location: /index_'.$best_lang.'.html');
exit;

/*

RFC 2616
https://www.w3.org/Protocols/rfc2616/rfc2616.html

10.	Status Code Definitions
		3.	Redirection 3xx
				1.	300 Multiple Choices
14.	Header Field Definitions
		4.	Accept-Language
		30.	Location

*/

?>
