[fr]
title: Capitaine Lycop
long_title: Capitaine Lycop : l’invasion des Héters
description: Un shooter spatial mariant aventure, action et gestion (développement en cours).
long_description:
	<p>L'humanité reçoit un signal provenant de l'espace. Un vaisseau extra-terrestre arrive. Un officier aguerri est désigné pour évaluer le danger.
	<p>Plongez au cœur de <em>Capitaine Lycop : L'invasion des Héters</em>, un shooter spatial mêlant aventure, action et gestion. Affrontez des insectes bureaucrates, des ennemis sans pitié et des boss épiques. Votre seul espoir est de bannir la lâcheté et d'emprunter un chemin de vertu.
body:
	<p><em>Capitaine Lycop : l'invasion des Héters</em> offre aux joueurs un shooter spatial avec une liberté complète de déplacement.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031699/D2709A87600DBDB87EF919D0F718E04C58F603CB/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031699/D2709A87600DBDB87EF919D0F718E04C58F603CB/">
	<p>Évoluez à travers plusieurs zones et un scénario complexe.
	<p>Équipez votre vaisseau de modules construits avec les débris d'autres vaisseaux.
	<p>Votre vaisseau dispose de sept modules orientables vous permettant d'adapter votre façon de jouer.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/284098101648963956/E8EFB32920CE410C3CE39A66EC3D882BE617A2FA/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/284098101648963956/E8EFB32920CE410C3CE39A66EC3D882BE617A2FA/">
	<p>Grâce à un arbre de technologies variées, revivez l'aventure plusieurs fois avec des stratégies de jeu différentes en choisissant parmi 31 technologies.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/284098101648963976/F412FBAB44B996AA9BCAFFF69B0EE72F8C37F92E/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/284098101648963976/F412FBAB44B996AA9BCAFFF69B0EE72F8C37F92E/">
	<p>Découvrez un univers extra-terrestre avec ses propres règles, mariant des pacifistes amicaux et pénibles, des administrations peu préoccupées par votre sort et des conquérants sans état d'âme.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031643/2385EDDF7C4159F2C182CE61F9DE3A1C2E2D489A/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031643/2385EDDF7C4159F2C182CE61F9DE3A1C2E2D489A/">
	<p>La fin de l'aventure sera déterminée par vos actions.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031668/E29BCCD357DF4C3631AD662605EEE2A1ECA4AEDE/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031668/E29BCCD357DF4C3631AD662605EEE2A1ECA4AEDE/">
	<p>Mais surtout vous devrez vaincre !
	<p>Vaincre des ennemis en quantité, commandés par des boss aussi ardus que hardis !
	<p>Vaincre sans peur ni reproche !
	<p>Car le capitaine Lycop n'est pas un lâche.

[en]
title: Captain Lycop
long_title: Captain Lycop: Invasion of the Heters
description: A space shooter combining adventure, action and management (work in progress).
long_description:
	<p>Mankind has received a transmission from space. An alien ship is coming. A hardened officer has been choosen to assess the danger.
	<p>Dive into the heart of <em>Captain Lycop: Invasion of the Heters</em>, a space shooter mixing adventure, action and management. Face off against pen-pushing insects, merciless enemies and epic bosses. Your only hope is to banish all cowardice and follow a virtuous path.
body:
	<p><em>Captain Lycop: Invasion of the Heters</em> gives players a space shooter with complete freedom of movement.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031699/D2709A87600DBDB87EF919D0F718E04C58F603CB/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031699/D2709A87600DBDB87EF919D0F718E04C58F603CB/">
	<p>Shoot your way through several zones and a complex story.
	<p>Scavenge ennemy ships to equip modules on your own.
	<p>Your ship holds up to seven modules pointed in any direction, allowing you to choose your play style.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/284098101648963956/E8EFB32920CE410C3CE39A66EC3D882BE617A2FA/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/284098101648963956/E8EFB32920CE410C3CE39A66EC3D882BE617A2FA/">
	<p>With a wide tech tree, replay the adventure with varying strategies choosing among 31 different technologies.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/284098101648963976/F412FBAB44B996AA9BCAFFF69B0EE72F8C37F92E/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/284098101648963976/F412FBAB44B996AA9BCAFFF69B0EE72F8C37F92E/">
	<p>Discover an alien universe with its own rules, mixing friendly yet annoying pacifists, administrators with little regard for your future and conquerors with no morals.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031643/2385EDDF7C4159F2C182CE61F9DE3A1C2E2D489A/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031643/2385EDDF7C4159F2C182CE61F9DE3A1C2E2D489A/">
	<p>The outcome of the adventure will depend on your choices.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031668/E29BCCD357DF4C3631AD662605EEE2A1ECA4AEDE/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031668/E29BCCD357DF4C3631AD662605EEE2A1ECA4AEDE/">
	<p>But most of all you must overcome!
	<p>Overcome hoards of enemies, led by bosses as feared as they are fearless!
	<p>Overcome with no fear!
	<p>Because Captain Lycop is no coward.

[de]
title: Captain Lycop
long_title: Captain Lycop: Invasion of the Heters
description: A space shooter combining adventure, action and management (work in progress).
long_description:
	<p>Die Menschheit erreicht eine Nachricht aus dem Weltall. Ein außerirdisches Schiff nähert sich. Doch ein stahlharter Offizier wurde auserwählt die Gefahr einzuschätzen.
	<p>Tauch ein in das Herz von Captain Lycop: Invasion of the Heters, ein einzigartiger Space Action Shooter, welcher Adventure, Action und Management-Elemente verbindet. Trotze bürokratischen Insekten, gnadenlosen Gegner und epischen Bossen. Deine einzige Hoffnung ist es jegliche Feigheit über Bord zu werfen und den Pfad der Tugend zu folgen.
body:
	<p><em>Captain Lycop: Invasion of the Heters</em> bietet den Spielern einen einzigartigen Shooter mit völliger Bewegungsfreiheit.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031699/D2709A87600DBDB87EF919D0F718E04C58F603CB/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031699/D2709A87600DBDB87EF919D0F718E04C58F603CB/">
	<p>Beweg dein Schiff durch diverse Zonen und durch eine komplexe Story.
	<p>Du kannst dein Schiff mit Modulen aus den Überresten anderer Schiffe erweitern.
	<p>Dein Schiff kann dabei mit bis zu sieben Modulen gleichzeitig bewaffnet sein. Diese können in jegliche Richtung ausgerichtet werden und ermöglichen dir somit das Spiel auf deine Weise zu spielen.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/284098101648963956/E8EFB32920CE410C3CE39A66EC3D882BE617A2FA/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/284098101648963956/E8EFB32920CE410C3CE39A66EC3D882BE617A2FA/">
	<p>Der umfrangreiche Tech Tree ermöglicht dir das Abenteuer mit verschiedenen Strategien immer wieder neu zu spielen. Wähle dabei aus 31 verschiedenen Technologien aus.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/284098101648963976/F412FBAB44B996AA9BCAFFF69B0EE72F8C37F92E/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/284098101648963976/F412FBAB44B996AA9BCAFFF69B0EE72F8C37F92E/">
	<p>Du wirst auf ein fremdes Universum treffen, welches seine eigenen Regeln hat: freundliche aber dennoch nervige Pazifisten, Administratoren mit nur wenig Mitleid für deine Zukunft und Eroberer ohne Moral.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031643/2385EDDF7C4159F2C182CE61F9DE3A1C2E2D489A/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031643/2385EDDF7C4159F2C182CE61F9DE3A1C2E2D489A/">
	<p>Der Ausgang des Abenteuers hängt von deinen Entscheidungen ab.
	<p><img src="http://images.akamai.steamusercontent.com/ugc/528388368573031668/E29BCCD357DF4C3631AD662605EEE2A1ECA4AEDE/400.x0.resizedimage" data-large="http://images.akamai.steamusercontent.com/ugc/528388368573031668/E29BCCD357DF4C3631AD662605EEE2A1ECA4AEDE/">
	<p>Aber insbesondere must du siegen!
	<p>Siege über Horden von Gegner, angeführt von gefürchteten und furchtlosen Bossen.
	<p>Siege ohne Furcht!
	<p>Denn Captain Lycop ist kein Feigling.

[DEFAULT]
icon: /lycop/icon.png
priority: 1
