#!/usr/bin/python
# coding: utf-8

from ConfigParser import RawConfigParser as ConfigParser
import os
import re
import time

base = 'http://aeronie.fr'
nodes = {}
news = []
langs = set()

texts = {
	'lang': {'fr': 'lire en français', 'en': 'read in English', 'de': 'auf Deutsch lesen'},
	'published': {'fr': 'Publié', 'en': 'Published', 'de': 'Veröffentlicht'},
	'in': {'fr': 'dans', 'en': 'in', 'de': 'im'},
	'more': {'fr': 'Lire la suite', 'en': 'More', 'de': 'Weiter'},
	'generated': {'fr': 'Page générée', 'en': 'Generated', 'de': 'Erzeugt'},
	}
date_formats = {
	'fr': (
		'<time datetime="%Y-%m-%d">le {0} {2} {1} %Y</time>',
		('lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'),
		('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'),
		['1er'] + range(2, 32),
		),
	'en': (
		'<time datetime="%Y-%m-%d">on {0}, {1} {2}, %Y</time>',
		('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'),
		('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
		[str(i) + ('th' if i > 3 and i < 21 else 'st' if i % 10 == 1 else 'nd' if i % 10 == 2 else 'rd' if i % 10 == 3 else 'th') for i in range(1, 32)],
		),
	'de': (
		'<time datetime="%Y-%m-%d">am {0} {2}. {1} %Y</time>',
		('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag'),
		('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'),
		range(1, 32),
		),
	'RSS': (
		'%d {1} %Y 12:00 UT',
		('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'),
		('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
		range(1, 32),
		),
	}

def format_date(date, lang):
	x = date_formats[lang]
	return time.strftime(x[0], date).format(x[1][date.tm_wday], x[2][date.tm_mon - 1], x[3][date.tm_mday - 1], *date)

def get_data(node, lang):
	for x in [lang, 'en', 'fr'] + list(langs):
		try: return node['data'][x], x
		except KeyError: pass

def quote_json(x): return x.replace('\\', '\\\\').replace('"', '\\"')
def quote_xml(x): return x.replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace('"', '&quot;')

def write_xml(_file, _text, *args, **kwargs): _file.write(re.sub('> <', '><', re.sub('\s+', ' ', _text.strip().format(*args, **kwargs))))

def is_real_page(node, data): return data['uri'].startswith(node['id'][1:])
def is_hidden(node): return not any('title' in data for lang, data in node['data'].iteritems())

def cmp_date(x, y): return cmp(y['date'], x['date'])
def cmp_priority_and_id(x, y): return cmp(y['priority'], x['priority']) or cmp(x['id'], y['id'])
def cmp_priority_and_date(x, y): return cmp(y['priority'], x['priority']) or cmp(y['date'], x['date'])

def collect(node):
	for child in node['children']:
		collect(child)
		node['news_0'] += child['news_0']
		node['news_1'] += child['news_1']
		node['news_2'] += child['news_2']
	node['children'].sort(cmp = cmp_priority_and_id)
	node['news_1'].sort(cmp = cmp_date)
	node['news_2'].sort(cmp = cmp_priority_and_date)

def main():
	for dir, dirs, files in os.walk('.'):
		for file in files:
			if file.endswith('.in'):
				file = os.path.join(dir, file)
				data = ConfigParser({'priority': 0, 'body': '', 'icon': '/style/star.svg', 'functions': ''})
				data.read(file)
				id = file = file[:-3]
				if id.endswith('/index'): id = id[:-6]
				nodes[id] = {'id': id, 'data': {}, 'children': [], 'news_0': [], 'news_1': [], 'news_2': [], 'priority': data.getint('DEFAULT', 'priority')}
				for lang in data.sections():
					langs.add(lang)
					x = nodes[id]['data'][lang] = dict(data.items(lang))
					if 'uri' not in x: x['uri'] = file[1:] + '_' + lang + '.html'
					if 'long_title' not in x: x['long_title'] = x['title']
					if 'long_description' not in x: x['long_description'] = '<p>' + quote_xml(x['description']) + '</p>'
	for id, node in nodes.iteritems():
		parent, child = os.path.split(id)
		if parent:
			try: node['date'] = time.strptime(child, "%Y-%m-%d")
			except ValueError:
				if not is_hidden(node): nodes[parent]['children'].append(node)
			else:
				nodes[parent]['news_' + str(cmp(node['priority'], 0) + 1)].append(node)
				news.append(node)
	news.sort(cmp = cmp_date)
	collect(nodes['.'])
	write_sitemap_file()
	for lang in langs:
		write_json_file(lang)
		write_atom_file(lang)
		write_rss_file(lang)
	for id, node in nodes.iteritems():
		for lang, data in node['data'].iteritems():
			if is_real_page(node, data): write_html_file(node, lang, data)

## HTML

html_begin = '''
	<!doctype html>
	<html lang="{lang}">
		<head>
			<meta charset="utf-8">
			<meta name="description" content="{description}">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link rel="stylesheet" type="text/css" href="/style.css">
			<link rel="alternate" type="application/rss+xml" href="/rss_{lang}.xml" title="Aéronie">
			<link rel="alternate" type="application/atom+xml" href="/atom_{lang}.xml" title="Aéronie">
'''
html_middle = '''
			<title>{title}</title>
		</head>
		<body>
			<header><a href="/"></a></header>
			<nav id="nav-top"><ul></ul></nav>
			<div id="body">
				<div id="body-inner">
					<h1>{title}</h1>
					{info}
					<header>{description}</header>
					{body}
'''
html_frame = '''
					<article>
						<img src="{icon}" alt="">
						<h2><a href="{uri}" hreflang="{lang}" lang="{lang}">{title}</a></h2>
						{info}
						<br>
						<p><span lang="{lang}">{description}</span>&ensp;<a href="{uri}" hreflang="{lang}">{label}...</a></p>
					</article>
'''
html_end = '''
				</div>
			</div>
			<nav id="nav-bottom"></nav>
			<footer>
				<p>Aéronie, SAS
				<p>2 rue des vignerons, 63110 Beaumont
				<p>Tél. <a href="tel:+33673329435">06 73 32 94 35</a>
				<p><a href="mailto:aeronie.studio@gmail.com">aeronie.studio@gmail.com</a>
				<p>© {year_now} Aéronie, tous droits réservés
				<p>{date_now}
			</footer>
			<script src="/data_{lang}.js"></script>
			<script src="/script.js"></script>
		</body>
	</html>
'''
def get_html_date(node, lang):
	if 'date' in node: return ' ' + format_date(node['date'], lang)
	return ''
def get_html_ancestors(node, root, lang):
	x = ''
	while node['id'] != '.':
		node = nodes[os.path.dirname(node['id'])]
		if node['id'] == root['id']: break
		data, real_lang = get_data(node, lang)
		x = ' / <a href="{uri}" hreflang="{lang}" lang="{lang}">{text}</a>'.format(uri = quote_xml(data['uri']), lang = real_lang, text = quote_xml(data['title'])) + x
	if x: return ' ' + texts['in'][lang] + x[2:]
	return ''
def get_html_info(node, root, lang):
	x = get_html_date(node, lang) + get_html_ancestors(node, root, lang)
	if x: return '<p>' + texts['published'][lang] + x + '</p>'
	return ''
def get_html_langs(node, lang):
	x = ''
	for other_lang, other_data in node['data'].iteritems():
		if other_lang != lang: x += ' | <a rel="alternate" href="{uri}" hreflang="{lang}" lang="{lang}">{text}</a>'.format(uri = quote_xml(other_data['uri']), lang = other_lang, text = texts['lang'][other_lang])
	if x: return '<p>' + x[3:] + '</p>'
	return ''
def write_html_node(file, node, root, lang):
	data, real_lang = get_data(node, lang)
	info = get_html_info(node, root, lang)
	if info: info = '<footer>' + info + '</footer>'
	write_xml(file, html_frame, uri = quote_xml(data['uri']), icon = quote_xml(data['icon']), title = quote_xml(data['long_title']), description = quote_xml(data['description']), info = info, label = texts['more'][lang], lang = real_lang)
def write_html_file(node, lang, data):
	for x in data['functions'].split(): functions[x](node)
	file = open('.' + data['uri'], 'w')
	write_xml(file, html_begin, description = quote_xml(data['description']), lang = lang)
	for other_lang, other_data in node['data'].iteritems():
		if other_lang != lang: write_xml(file, '<link rel="alternate" href="{uri}" hreflang="{lang}">', uri = quote_xml(other_data['uri']), lang = other_lang)
	info = get_html_info(node, nodes['.'], lang) + get_html_langs(node, lang)
	if info: info = '<footer>' + info + '</footer>'
	write_xml(file, html_middle, title = quote_xml(data['long_title']), info = info, description = data['long_description'], body = data['body'])
	for child in node['news_2']: write_html_node(file, child, node, lang)
	for child in node['children']: write_html_node(file, child, node, lang)
	for child in node['news_1']: write_html_node(file, child, node, lang)
	write_xml(file, html_end, lang = lang, date_now = texts['generated'][lang] + ' ' + format_date(time.gmtime(), lang), year_now = time.gmtime().tm_year)

## NEWS

def function_news(node): node.update(id = '.', news_1 = news)
functions = {'news': function_news}

## JSON

def write_json_node(file, node, lang):
	data, real_lang = get_data(node, lang)
	file.write('{{t:"{}",u:"{}",l:"{}"'.format(quote_json(data['title']), quote_json(data['uri']), real_lang))
	if node['children']:
		file.write(',c:[')
		for child in node['children']:
			write_json_node(file, child, lang)
			file.write(',')
		file.write(']')
	file.write('}')
def write_json_file(lang):
	file = open('data_' + lang + '.js', 'w')
	file.write('var contents=')
	write_json_node(file, nodes['.'], lang)

## SITEMAP

sitemap_begin = '''
	<?xml version="1.0" encoding="utf-8"?>
	<urlset
		xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
		xmlns:xhtml="http://www.w3.org/1999/xhtml"
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
'''
sitemap_entry_begin = '''
		<url>
			<loc>{uri}</loc>
			<lastmod>{date}</lastmod>
			<changefreq>daily</changefreq>
			<priority>{priority}</priority>
'''
sitemap_entry_end = '''
		</url>
'''
sitemap_end = '''
	</urlset>
'''
def write_sitemap_file():
	file = open('sitemap.xml', 'w')
	write_xml(file, sitemap_begin)
	for id, node in nodes.iteritems():
		if 'date' in node: continue
		for lang, data in node['data'].iteritems():
			if not is_real_page(node, data): continue
			write_xml(file, sitemap_entry_begin, uri = quote_xml(base + data['uri']), date = time.strftime('%Y-%m-%d'), priority = .5 ** data['uri'].count('/'))
			if len(node['data']) > 1:
				for other_lang, other_data in node['data'].iteritems(): write_xml(file, '<xhtml:link rel="alternate" href="{uri}" hreflang="{lang}"/>', uri = quote_xml(base + other_data['uri']), lang = other_lang)
			write_xml(file, sitemap_entry_end)
	write_xml(file, sitemap_end)

## ATOM

atom_begin = '''
	<?xml version="1.0" encoding="utf-8"?>
	<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="{lang}">
		<title>{title}</title>
		<subtitle>{description}</subtitle>
		<updated>{date}</updated>
		<link rel="self" href="{self}"/>
		<id>{self}</id>
		<author>
			<name>Aéronie</name>
			<email>aeronie.studio@gmail.com</email>
		</author>
'''
atom_entry = '''
		<entry xml:lang="{lang}">
			<title>{title}</title>
			<summary>{description}</summary>
			<updated>{date}</updated>
			<link rel="alternate" href="{uri}"/>
			<id>{uri}</id>
		</entry>
'''
atom_end = '''
	</feed>
'''
def write_atom_file(lang):
	file = open('atom_' + lang + '.xml', 'w')
	data, real_lang = get_data(nodes['.'], lang)
	write_xml(file, atom_begin, date = time.strftime('%Y-%m-%dT12:00:00Z', time.gmtime()), self = quote_xml(base + '/' + file.name), title = quote_xml(data['long_title']), description = quote_xml(data['description']), lang = real_lang)
	for node in news:
		data, real_lang = get_data(node, lang)
		write_xml(file, atom_entry, date = time.strftime('%Y-%m-%dT12:00:00Z', node['date']), uri = quote_xml(base + data['uri']), title = quote_xml(data['long_title']), description = quote_xml(data['description']), lang = real_lang)
	write_xml(file, atom_end)

## RSS

rss_begin = '''
	<?xml version="1.0" encoding="utf-8"?>
	<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
		<channel>
			<title>{title}</title>
			<description>{description}</description>
			<lastBuildDate>{date}</lastBuildDate>
			<link>{uri}</link>
			<atom:link rel="self" href="{self}"/>
'''
rss_entry = '''
			<item>
				<title>{title}</title>
				<description>{description}</description>
				<pubDate>{date}</pubDate>
				<link>{uri}</link>
				<guid>{uri}</guid>
			</item>
'''
rss_end = '''
		</channel>
	</rss>
'''
def write_rss_file(lang):
	file = open('rss_' + lang + '.xml', 'w')
	data, real_lang = get_data(nodes['.'], lang)
	write_xml(file, rss_begin, date = format_date(time.gmtime(), 'RSS'), self = quote_xml(base + '/' + file.name), uri = quote_xml(base + data['uri']), title = quote_xml(data['long_title']), description = quote_xml(data['description']))
	for node in news:
		data, real_lang = get_data(node, lang)
		write_xml(file, rss_entry, date = format_date(node['date'], 'RSS'), uri = quote_xml(base + data['uri']), title = quote_xml(data['long_title']), description = quote_xml(data['description']))
	write_xml(file, rss_end)

##

if __name__ == '__main__': main()
