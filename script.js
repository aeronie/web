function create_nav(x)
{
	var li = document.createElement('li')
	var a = document.createElement('a')
	a.setAttribute('href', x.u)
	a.setAttribute('lang', x.l)
	a.appendChild(document.createTextNode(x.t))
	li.appendChild(a)
	if(x.c)
	{
		var ul = document.createElement('ul')
		for(var i in x.c) ul.appendChild(create_nav(x.c[i]))
		li.appendChild(ul)
	}
	return li
}
function zoom_img()
{
	var x = this.getAttribute('data-large')
	this.setAttribute(x ? 'data-small' : 'data-large', this.getAttribute('src'))
	this.setAttribute('src', this.getAttribute(x ? 'data-large' : 'data-small'))
	this.removeAttribute(x ? 'data-large' : 'data-small')
}

var ul = document.getElementById('nav-top').firstChild
ul.appendChild(create_nav(contents))
document.getElementById('nav-bottom').appendChild(ul.cloneNode(true))

var nodes = document.querySelectorAll('img[data-large]')
for(var i in nodes) nodes[i].onclick = zoom_img
